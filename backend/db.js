const mysql = require('mysql')

const openConnection = ()=>{
    console.log('creating connection')
    const connection = mysql.createConnection({
        uri: 'mysql://db:3306',
        user:'root',
        password:'root',
        database:'myemp'
    })

    console.log('creating connection')

    connection.connect()

    return connection
}

module.exports= {openConnection}
